// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt

import "net/http"

// Options used to alter the behavior of a test step.
type Options struct {
	// Before if set will be run before the step is executed. If an error is
	// returned the test will fail and the step will not be run.
	Before func(stepNumber int, step *Step) error `yaml:"-"`

	// Execute if set will be executed to retrieve the result of the
	// operation from the GraphQL API. An error should be returned only if the
	// response couldn't be retrieved, for example due to a connection issue.
	Execute func(*Step) (result map[string]interface{}, err error) `yaml:"-"`

	// After if set will be run after the step is executed. If an error is
	// returned the test will fail. result and err are the return values of
	// executing the step.
	After func(stepNumber int, step *Step, result map[string]interface{}, err error) error `yaml:"-"`

	// Endpoint is the endpoint of the GraphQL API.
	Endpoint string `yaml:"endpoint"`
	// Header to be included with the request. Additional headers may be added
	// to this field by the Execute func.
	Header http.Header `yaml:"header"`

	// Method alters how the request is sent to the endpoint. Acceptable values are
	// "", "post", "get".
	//
	// The empty string or "post" will use an HTTP POST with a JSON body
	// containing the operation, query, and variables.
	//
	// "get" will use an HTTP GET request with the operation, query, and
	// variables encoded as URL query parameters.
	Method string `yaml:"method"`

	// Custom allows specifying custom configuration which may be useful when
	// using custom Before, Execute, or After funcs. This is not used inside this
	// package.
	Custom interface{} `yaml:"custom"`

	// AllowErrors if true will not fail a test step if there are errors in the
	// returned GraphQL response. Useful for making assertions about error
	// conditions.
	AllowErrors bool `yaml:"allowErrors"`

	// Quiet if true will suppress request/response output on a successful run.
	Quiet bool `yaml:"quiet"`
}

// DefaultOptions used by this package.
func DefaultOptions() Options {
	return Options{
		Method: "post",
	}
}

// mergeOptions merges values from b into a when the value in a is zero.
func mergeOptions(a, b *Options) {
	if a.Before == nil {
		a.Before = b.Before
	}
	if a.Execute == nil {
		a.Execute = b.Execute
	}
	if a.After == nil {
		a.After = b.After
	}
	if a.Endpoint == "" {
		a.Endpoint = b.Endpoint
	}
	for k := range b.Header {
		if a.Header == nil {
			a.Header = http.Header{k: b.Header[k]}
		} else {
			a.Header[k] = b.Header[k]
		}
	}
	if a.Method == "" {
		a.Method = b.Method
	}
	if a.Custom == nil {
		a.Custom = b.Custom
	}
	if !a.AllowErrors {
		a.AllowErrors = b.AllowErrors
	}
	if !a.Quiet {
		a.Quiet = b.Quiet
	}
}
