// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt_test

import (
	"fmt"
	"net/http"
	"os"

	"github.com/fatih/color"
	"github.com/ohler55/ojg/oj"
	"gopkg.in/yaml.v3"
)

// graphqlMock provides a mock GraphQL API server.
type graphqlMock struct {
	n         int
	responses []mockResponse
}

func newGraphQLMock(mockResponsesFile string) (*graphqlMock, error) {
	bytes, err := os.ReadFile(mockResponsesFile)
	if err != nil {
		return nil, err
	}
	var responses []mockResponse
	if err = yaml.Unmarshal(bytes, &responses); err != nil {
		return nil, err
	}
	return &graphqlMock{
		n:         0,
		responses: responses,
	}, nil

}

// ServeHTTP handles an HTTP request.
func (g *graphqlMock) ServeHTTP(res http.ResponseWriter, _ *http.Request) {
	resp := g.responses[g.n]
	bodyBytes, err := oj.Marshal(resp.Body)
	if err != nil {
		res.WriteHeader(500)
		color.Red("mock: " + err.Error())
		return
	}
	res.Header().Set("Content-type", "application/json")
	res.WriteHeader(resp.Code)
	fmt.Fprintln(res, string(bodyBytes))
	g.n++
}

type mockResponse struct {
	Code int
	Body interface{}
}
