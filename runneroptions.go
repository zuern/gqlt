// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt

import (
	"io"
	"os"
)

// RunnerOptions are options to configure the test Runner.
type RunnerOptions struct {
	// Verbose makes tests print output on successful test steps as well as
	// failed ones.
	Verbose bool

	IncludeTestName     bool
	IncludeStepName     bool
	IncludeURL          bool
	IncludeHeaders      bool
	IncludeRequestBody  bool
	IncludeResponseBody bool
	IncludeDiff         bool

	// Output from the Runner will be written here.
	Output io.Writer
}

// DefaultRunnerOptions used by this package.
func DefaultRunnerOptions() *RunnerOptions {
	return &RunnerOptions{
		Verbose:             true,
		IncludeTestName:     true,
		IncludeStepName:     true,
		IncludeURL:          true,
		IncludeHeaders:      true,
		IncludeRequestBody:  true,
		IncludeResponseBody: true,
		IncludeDiff:         true,
		Output:              os.Stdout,
	}
}
