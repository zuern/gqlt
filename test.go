// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt

import "fmt"

// A Test to be executed.
type Test struct {
	// Name of the test.
	Name string `yaml:"name"`
	// Description of the test.
	Description string `yaml:"description"`
	// Test steps to run.
	Steps []*Step `yaml:"steps"`
	// Options to apply to all steps in the test by default.
	Options Options `yaml:"options"`
}

// Validate the test and return any validation errors.
func (t Test) Validate() (errs []string) {
	if len(t.Steps) == 0 {
		errs = append(errs, "A test must have at least one step")
	}
	for i, step := range t.Steps {
		for _, err := range step.Validate() {
			errs = append(errs, fmt.Sprintf("Step %d: %v", i, err))
		}
	}
	return errs
}
