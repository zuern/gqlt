GQLT: GraphQL Tester
========================

Blackbox GraphQL API testing tool inspired by
[graphql-test-tool](https://github.com/ohler55/graphql-test-tool)
