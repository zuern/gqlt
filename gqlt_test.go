// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt_test

import (
	"net/http/httptest"
	"testing"

	"gitlab.com/zuern/gqlt"
)

func TestGQLT(t *testing.T) {
	mock, err := newGraphQLMock("testdata/testgqltmockresponses.yaml")
	if err != nil {
		t.Fatal(err)
	}
	srv := httptest.NewServer(mock)
	defer srv.Close()

	runner := gqlt.NewRunner(t, &gqlt.RunnerOptions{
		Verbose: testing.Verbose(),
	})

	test := runner.LoadTestFile("testdata/testcase.yaml")
	test.Options.Endpoint = srv.URL

	runner.RunTest(test)
}
