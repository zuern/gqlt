// Copyright 2021 Kevin Zuern. All rights reserved.

package gqlt

// A Step in a test.
type Step struct {
	// Name of the step.
	Name string `yaml:"name"`
	// GraphQL Query to send to the endpoint.
	Query string `yaml:"query"`
	// Op is the operation name.
	Op string `yaml:"op"`
	// Vars are a map of GraphQL variables.
	Vars map[string]interface{} `yaml:"vars"`
	// Expect is the expected return value from the API. The step will fail if
	// errors are present in the returned API response, unless
	// Options.AllowErrors is set to true. If Expect is nil the step will only
	// check for the absence of errors.
	Expect map[string]interface{} `yaml:"expect"`
	// Options to override test Options for this step only.
	Options Options `yaml:"options,omitempty"`
}

// Validate returns a list of validation errors if any.
func (s Step) Validate() (errs []string) {
	if len(s.Expect) == 0 && s.Options.AllowErrors {
		errs = append(errs, "Expect map cannot be nil/empty with AllowErrors option true (test would always pass).")
	}
	if s.Options.Execute == nil { // using default execute func
		if s.Options.Endpoint == "" {
			errs = append(errs, "Endpoint cannot be empty.")
		}
	}
	return errs
}
