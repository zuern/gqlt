module gitlab.com/zuern/gqlt

go 1.16

require (
	github.com/fatih/color v1.12.0
	github.com/ohler55/ojg v1.12.0
	github.com/pmezard/go-difflib v1.0.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
