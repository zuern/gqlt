// Copyright 2021 Kevin Zuern. All rights reserved.
package gqlt

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/ohler55/ojg"
	"github.com/ohler55/ojg/oj"
	"github.com/pmezard/go-difflib/difflib"
	"gopkg.in/yaml.v3"
)

var ojOptions = &ojg.Options{
	Sort:        true,
	Indent:      2,
	SyntaxColor: ojg.Normal,
	KeyColor:    ojg.Blue,
	NullColor:   ojg.Magenta,
	BoolColor:   ojg.Yellow,
	NumberColor: ojg.Cyan,
	StringColor: ojg.Green,
	TimeColor:   ojg.Green,
}

// A Runner runs a test.
type Runner struct {
	t         testing.TB
	opts      RunnerOptions
	logBuffer bytes.Buffer
}

// NewRunner creates a new test Runner. If options is nil DefaultRunnerOptions
// is used.
func NewRunner(t testing.TB, options *RunnerOptions) *Runner {
	if options == nil {
		options = DefaultRunnerOptions()
	}
	return &Runner{
		t:    t,
		opts: *options,
	}
}

// LoadTestFile loads a Test from yamlFilePath.
func (r *Runner) LoadTestFile(yamlFilePath string) *Test {
	r.t.Helper()
	bytes, err := os.ReadFile(yamlFilePath)
	if err != nil {
		r.t.Fatalf("read test file: %v", err)
	}
	var test Test
	if err = yaml.Unmarshal(bytes, &test); err != nil {
		r.t.Fatalf("parse test file: %v", err)
	}
	return &test
}

// RunTest validates and executes the test. If a step fails, the runner will
// fail the Go test. RunTest must be invoked from the main goroutine running
// the test.
func (r *Runner) RunTest(test *Test) {
	r.t.Helper()
	t := r.t
	if r.opts.Output == nil {
		r.opts.Output = os.Stdout
	}
	defer func() {
		if t.Failed() || r.opts.Verbose {
			io.Copy(r.opts.Output, &r.logBuffer)
		}
	}()
	defaults := DefaultOptions()
	mergeOptions(&test.Options, &defaults)
	for _, step := range test.Steps {
		mergeOptions(&step.Options, &test.Options)
	}
	if errs := test.Validate(); len(errs) > 0 {
		t.Fail()
		r.logc(color.FgRed, "Test failed to validate. Errors: \n")
		for _, err := range errs {
			r.logc(color.FgRed, "- %v\n", err)
		}
		t.FailNow()
		return
	}
	if r.opts.IncludeTestName {
		r.log("%s\n", test.Name)
	}
	const stars = "************************************************************************************************************************"
	for stepNumber, step := range test.Steps {
		var err error
		if r.opts.IncludeStepName {
			const width = 79
			start := "********** " + step.Name
			r.log(start)
			if len(start) < width-1 {
				r.log(" %s", string(stars[:width-len(start)-1]))
			}
			r.log("\n")
		}

		// Before.
		if f := step.Options.Before; f != nil {
			if err = f(stepNumber, step); err != nil {
				r.logc(color.FgRed, "Before func failed: %v\n", err)
				t.FailNow()
				return
			}
		}

		// Execute.
		var result map[string]interface{}
		if step.Options.Execute == nil {
			result, err = r.executeStep(step)
		} else {
			result, err = step.Options.Execute(step)
		}
		if err != nil {
			r.logc(color.FgRed, "Failed to execute step: %v\n", err)
			t.FailNow()
			return
		}
		expect := oj.JSON(step.Expect, ojOptions)
		actual := oj.JSON(result, ojOptions)
		if r.opts.IncludeResponseBody && !step.Options.Quiet {
			opts := *ojOptions
			opts.Color = !color.NoColor
			r.log("%s\n", oj.JSON(result, &opts))
		}
		if errs, _ := result["errors"].([]interface{}); len(errs) > 0 && !step.Options.AllowErrors {
			r.logc(color.FgRed, "FAILED: GraphQL errors returned\n")
			t.FailNow()
			return
		}
		// Only make assertions about equality if step expected certain data back.
		if len(step.Expect) > 0 && !strings.EqualFold(expect, actual) {
			t.Fail()
			r.logc(color.FgRed, "FAILED: Expectation mismatch:\n")
			diff, err := difflib.GetUnifiedDiffString(difflib.UnifiedDiff{
				A:        difflib.SplitLines(expect),
				B:        difflib.SplitLines(actual),
				FromFile: "Expected",
				ToFile:   "Actual",
				Context:  1,
			})
			if err != nil {
				r.logc(color.FgRed, "generating diff failed: %v\n", err)
			} else {
				var c color.Attribute
				for _, line := range strings.Split(diff, "\n") {
					c = color.Reset
					if len(line) > 0 {
						switch line[0] {
						case '-':
							c = color.FgGreen
						case '+':
							c = color.FgRed
						case '@':
							if len(line) > 1 && line[1] == '@' {
								c = color.FgCyan
							}
						}
					}
					r.logc(c, "%s\n", line)
				}
			}
		}

		// After.
		if f := step.Options.After; f != nil {
			if err = f(stepNumber, step, result, err); err != nil {
				r.logc(color.FgRed, "After func failed: %v\n", err)
				t.FailNow()
				return
			}
		}
	}
}

// executeStep is the function called by default to execute a test step and
// fetch the result.
func (r *Runner) executeStep(s *Step) (result map[string]interface{}, err error) {
	ctx, cf := context.WithTimeout(context.Background(), 30*time.Second)
	defer cf()

	var (
		endpoint *url.URL
		query    url.Values
		header   = s.Options.Header.Clone()
		method   string
		body     bytes.Buffer
	)
	if header == nil {
		header = http.Header{}
	}
	if endpoint, err = url.Parse(s.Options.Endpoint); err != nil {
		return nil, fmt.Errorf("parse endpoint: %w", err)
	}
	query = endpoint.Query()
	switch s.Options.Method {
	case "get":
		method = http.MethodGet
		query.Add("operationName", s.Op)
		query.Add("query", s.Query)
		if len(s.Vars) > 0 {
			var varsBytes []byte
			if varsBytes, err = oj.Marshal(s.Vars); err != nil {
				return nil, fmt.Errorf("marshal GraphQL variables: %w", err)
			}
			query.Add("variables", string(varsBytes))
		}
		endpoint.RawQuery = query.Encode()
	case "", "post":
		method = http.MethodPost
		header.Add("Content-Type", "application/json")
		body.WriteString(
			oj.JSON(map[string]interface{}{
				"operationName": s.Op,
				"query":         s.Query,
				"variables":     s.Vars,
			}, 2),
		)
	default:
		return nil, fmt.Errorf("invalid mode option: %q", s.Options.Method)
	}
	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, method, endpoint.String(), &body); err != nil {
		return nil, fmt.Errorf("build request: %w", err)
	}
	req.Header = header

	if r.opts.IncludeURL {
		r.logc(color.FgGreen, "%s: %s\n", req.Method, endpoint.String())
	}
	if len(req.Header) > 0 && r.opts.IncludeHeaders && !s.Options.Quiet {
		var buf bytes.Buffer
		req.Header.Write(&buf)
		r.logc(color.FgGreen, buf.String())
	}
	if body.Len() > 0 && r.opts.IncludeRequestBody && !s.Options.Quiet {
		r.logc(color.FgYellow, "%s\n", body.String())
	}

	if r.opts.Verbose && s.Options.Quiet {
		r.logc(color.FgYellow, "Request/Response suppressed by Quiet option\n")
	}

	var resp *http.Response
	if resp, err = http.DefaultClient.Do(req); err != nil {
		return nil, fmt.Errorf("fetch api response: %w", err)
	}
	defer resp.Body.Close()
	var respBytes []byte
	if respBytes, err = io.ReadAll(resp.Body); err != nil {
		return nil, fmt.Errorf("read api response: %w", err)
	}
	if err = oj.Unmarshal(respBytes, &result); err != nil {
		return nil, fmt.Errorf("unmarshal api response: %w\nResponse body was:\n%s\n", err, string(respBytes))
	}
	return result, err
}

func (r *Runner) log(format string, args ...interface{}) {
	fmt.Fprintf(&r.logBuffer, format, args...)
}

func (r *Runner) logc(c color.Attribute, format string, args ...interface{}) {
	color.New(c).FprintfFunc()(&r.logBuffer, format, args...)
}
