# [0.0.6] - 2021-08-23
## Fixed
- Response body output is colorized only when other output will be colorized
  too.

# [0.0.5] - 2021-08-19
## Added
- Quiet option to suppress request/response output for single steps.

# [0.0.4] - 2021-07-13
## Changed
- Switch runner options from Omit* to Include*
- Formatting fixes and better coloring of diff output.
## Fixed
- Bugfix: ensure colored log output gets printed when not verbose and test fails

# [0.0.3] - 2021-07-13
## Changed
- Colorize JSON output.
- Print response bytes in case Unmarshal of response body fails.
- Allow empty expect, only checks for errors in that case.
- Rename Mode option to Method.

# [0.0.2] - 2021-07-03
## Added
- License
- Copyright notices

# [0.0.1] - 2021-07-03
## Added
- Initial commit.
